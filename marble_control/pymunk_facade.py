"""Pymunk facade to interact with marble_control data structures."""
import random
import pymunk
from .marble import Marble
from .polygon import ConvexPolygon


# Waypoints do not collide with anything
WAYPOINT_FILTER = pymunk.ShapeFilter(mask=0b0)


def add_marble(space: pymunk.Space, marble: Marble):
    """Add the marble to the space."""
    inertia = pymunk.moment_for_circle(
            marble.mass, 0, marble.radius, (0, 0)
        )
    body = pymunk.Body(marble.mass, inertia)
    shape = pymunk.Circle(body, marble.radius)
    shape.elasticity = marble.elasticity
    shape.friction = marble.radius
    body.position = marble.position
    body.velocity = marble.velocity
    space.add(body, shape)


def add_polygon(space: pymunk.Space, polygon: ConvexPolygon):
    """Add the polygon to the space."""
    body = pymunk.Body(body_type=pymunk.Body.STATIC)
    shape = pymunk.Poly(body, polygon.vertices)
    shape.elasticity = polygon.elasticity
    shape.friction = polygon.friction
    space.add(body, shape)


def find_empty_space(
        position_range: tuple[float, float],
        radius: float,
        space: pymunk.Space,
        shape_filter: pymunk.ShapeFilter,
        ) -> tuple[float, float]:
    """Return a position that is empty in the given space. The position is
    around the origin within the given horizontal and vertical range."""
    point = (
            random.uniform(-position_range[0], position_range[0]),
            random.uniform(-position_range[1], position_range[1])
        )
    while not space.point_query_nearest(
            point,
            radius,
            shape_filter
            ) is None:
        point = (
                random.uniform(-position_range[0], position_range[0]),
                random.uniform(-position_range[1], position_range[1])
            )
    return point
