"""A marble is a kinematic circle."""
from dataclasses import dataclass


@dataclass
class Marble:
    """A circle with physical properties."""
    position: tuple[float, float]
    velocity: tuple[float, float]
    mass: float
    radius: float
    elasticity: float
