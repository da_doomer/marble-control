"""A waypoint is a circle."""
from dataclasses import dataclass


@dataclass(frozen=True, eq=True)
class Waypoint:
    """A target circular area and velocity."""
    position: tuple[float, float]
    radius: float
    velocity: tuple[float, float]
